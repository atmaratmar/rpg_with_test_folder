﻿using Assignment.Equipments;

namespace Assignment.Characters.Services
{
   public class Weapon :Item
    {
        public WeaponTypes WeaponType { get; set; }
        public WeaponAttributes WeaponAttributes { get; set; } = new();
    }
}
