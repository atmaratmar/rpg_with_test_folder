﻿using Assignment.Characters;
using Assignment.Characters.Services;

namespace Assignment.Equipments
{
  public class Armor :Item
    {
        public ArmorTypes ArmorType { get; set; }
        public PrimaryAttributes ArmorAttributes { get; set; } = new();
    }
}
