﻿namespace Assignment.Characters
{
    public class SecondaryAttributes
    {
        /// <summary>
        /// Health – determines how much overall damage a character can take before they die.
        ///o Each point of vitality increases health by 10.
        ///o Health = (Total Vitality) *10
        /// </summary>
        public int Health { get; set; }
        public int ArmorRating { get; set; }
        public int ElementalResistance { get; set; }


    }
}
