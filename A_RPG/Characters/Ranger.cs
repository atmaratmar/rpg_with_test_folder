﻿using Assignment.Equipments;
using System.Collections.Generic;
using System.Diagnostics;
using Assignment.Characters.Services;

namespace Assignment.Characters
{
    public  class Ranger : Hero
    {
        public Ranger(string name) : base(name)
        {
            SetStartsStats();
        }
        /// <summary>
        /// level up 
        /// </summary>
        public override void LevelUp()
        {
            Level++;
            ArmorTypes = new List<ArmorTypes>
            {
                Equipments.ArmorTypes.PLATE,
                Equipments.ArmorTypes.MAIL
            };
            WeaponTypes = new List<WeaponTypes>
            {
                Equipments.WeaponTypes.AXE,
                Equipments.WeaponTypes.HAMMER,
                Equipments.WeaponTypes.SWORD
            };
            PrimAttributes.Vitality += 2;
            PrimAttributes.Strength += 1;
            PrimAttributes.Dexterity += 5;
            PrimAttributes.Intelligence += 1;
        }
        /// <summary>
        /// Initial state
        /// </summary>
        public override void SetStartsStats()
        {
            PrimAttributes.Vitality = 8;
            PrimAttributes.Strength = 1;
            PrimAttributes.Dexterity = 7;
            PrimAttributes.Intelligence = 1;
            ArmorTypes = new List<ArmorTypes>
            {
                Equipments.ArmorTypes.LEATHER,
                Equipments.ArmorTypes.MAIL
            };
            WeaponTypes = new List<WeaponTypes>
            {
                Equipments.WeaponTypes.BOW,
            };

        }
        /// <summary>
        /// Secondary Attributes
        /// </summary>
        public override void SecondaryAttributesState()
        {
            SecondaryAttributes.Health = PrimAttributes.Vitality * 10;
            SecondaryAttributes.ArmorRating = PrimAttributes.Strength + PrimAttributes.Dexterity;
            SecondaryAttributes.ElementalResistance = PrimAttributes.Intelligence;
        }
        /// <summary>
        /// Calculate DPS
        /// </summary>
        public override void CalculateDps()
        {
            Weapon equippedWeapon = EquipmentTools[Slots.WEAPON] as Weapon;
            Debug.Assert(equippedWeapon != null, nameof(equippedWeapon) + " != null");
            Dps = (equippedWeapon.WeaponAttributes.AttackSpeed * equippedWeapon.WeaponAttributes.Damage) *
                  (1 + PrimAttributes.Strength / 100.0);
        }
    }
}
