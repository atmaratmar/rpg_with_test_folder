﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Assignment.Characters.Services;
using Assignment.Equipments;

namespace Assignment.Characters
{
   public class Warrior :Hero
   {
       
        public Warrior(string name) : base(name)
        {
            SetStartsStats();
        }
        /// <summary>
        /// level up 
        /// </summary>
        public override void LevelUp()
        {
            Level++;
            PrimAttributes.Vitality += 5;
            PrimAttributes.Strength += 3;
            PrimAttributes.Dexterity += 2;
            PrimAttributes.Intelligence += 1;
        }
        /// <summary>
        /// Initial state
        /// </summary>
        public sealed override void SetStartsStats()
        {
            ArmorTypes = new List<ArmorTypes>
            {
                Equipments.ArmorTypes.PLATE,
                Equipments.ArmorTypes.MAIL
            };
            WeaponTypes = new List<WeaponTypes>
            {
                Equipments.WeaponTypes.AXE,
                Equipments.WeaponTypes.HAMMER,
                Equipments.WeaponTypes.SWORD
            };
            PrimAttributes.Vitality = 10;
            PrimAttributes.Strength = 5;
            PrimAttributes.Dexterity = 2;
            PrimAttributes.Intelligence = 1;
            CalculateDps();
        }
        /// <summary>
        /// Secondary Attributes
        /// </summary>
        public override void SecondaryAttributesState()
        {
            SecondaryAttributes.Health = PrimAttributes.Vitality * 10;
            SecondaryAttributes.ArmorRating = PrimAttributes.Strength + PrimAttributes.Dexterity;
            SecondaryAttributes.ElementalResistance = PrimAttributes.Intelligence;
        }
        /// <summary>
        /// Calculate DPS
        /// </summary>

        public override void CalculateDps()
        {
            Weapon equippedWeapon = EquipmentTools[Slots.WEAPON] as Weapon;
            Debug.Assert(equippedWeapon != null, nameof(equippedWeapon) + " != null");
            Dps = (equippedWeapon.WeaponAttributes.AttackSpeed * equippedWeapon.WeaponAttributes.Damage) *
                  (1 + PrimAttributes.Strength / 100.0);
        }
        /// <summary>
        /// Display to the console
        /// </summary>
        /// <returns></returns>
        public StringBuilder PrintCharacterStats()
        {
            SetStartsStats();
            SecondaryAttributesState();
            CalculateDps();
            StringBuilder characterStats = new StringBuilder("Character Stats:\n");
            characterStats.AppendLine();
            characterStats.AppendLine($"Name: {Name}");
            characterStats.AppendLine($"Level: {Level}");
            characterStats.AppendLine();
            characterStats.AppendLine("Primary Stats:");
            characterStats.AppendLine($"\tStrength: {PrimAttributes.Strength}");
            characterStats.AppendLine($"\tDexterity: {PrimAttributes.Dexterity}");
            characterStats.AppendLine($"\tIntelligence: {PrimAttributes.Intelligence}");
            characterStats.AppendLine();
            characterStats.AppendLine("Secondary Stats:");
            characterStats.AppendLine($"\tHealth: {SecondaryAttributes.Health}");
            characterStats.AppendLine($"\tArmor Rating: {SecondaryAttributes.ArmorRating}");
            characterStats.AppendLine($"\tElemental Resistance: {SecondaryAttributes.ElementalResistance}");
            characterStats.AppendLine($"DPS: {Dps}");
            return characterStats;
        }
    }
}
