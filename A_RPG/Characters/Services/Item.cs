﻿using Assignment.Equipments;

namespace Assignment.Characters.Services
{
    public class Item
    {
        public string ItemName { get; set; }
        public int ItemLevel { get; set; }
        public Slots ItemSlot { get; set; }
    }
}
