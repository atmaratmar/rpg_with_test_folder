﻿using Assignment.Characters;
using System;

namespace Assignment
{
    class Program
    {
        static void Main(string[] args)
        {
            Warrior warrior = new Warrior("warrior");
            Console.WriteLine(warrior.PrintCharacterStats());
            
        }
    }
}
