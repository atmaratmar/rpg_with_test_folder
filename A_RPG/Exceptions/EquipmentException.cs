﻿using System;

namespace Assignment.Exceptions
{
   public class EquipmentException : Exception
    {
        public EquipmentException(string message) : base(message) { }
        public override string Message => "Some thing went wrong";
    }
}
