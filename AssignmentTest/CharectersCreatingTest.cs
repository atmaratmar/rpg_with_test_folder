using Assignment.Characters;
using Xunit;

namespace AssignmentTest
{
    public class CharactersAssignmentTest
    {
        [Fact]
        public void Test_Mage_state_and_return_initial_state()
        {
            Mage mage = new Mage("Mage");
            Assert.Equal(5, mage.PrimAttributes.Vitality);
            Assert.Equal(1, mage.PrimAttributes.Strength);
            Assert.Equal(1, mage.PrimAttributes.Dexterity);
            Assert.Equal(8, mage.PrimAttributes.Intelligence);
        }
        [Fact]
        public void Test_Mage_levelUp_state_and_return_levelUp_state()
        {
            Mage mage = new Mage("Mage");
            mage.LevelUp();
            Assert.Equal(8, mage.PrimAttributes.Vitality);
            Assert.Equal(2, mage.PrimAttributes.Strength);
            Assert.Equal(2, mage.PrimAttributes.Dexterity);
            Assert.Equal(13, mage.PrimAttributes.Intelligence);
        }
        [Fact]
        public void Test_Rogue_state_and_return_initial_state()
        {
            Rogue rogue = new Rogue("Rogue");
            Assert.Equal(8, rogue.PrimAttributes.Vitality);
            Assert.Equal(2, rogue.PrimAttributes.Strength);
            Assert.Equal(6, rogue.PrimAttributes.Dexterity);
            Assert.Equal(1, rogue.PrimAttributes.Intelligence);
        }
        [Fact]
        public void Test_Rogue_levelUp_state_and_return_levelUp_state()
        {
            Rogue rogue = new Rogue("Rogue");
            rogue.LevelUp();
            Assert.Equal(11, rogue.PrimAttributes.Vitality);
            Assert.Equal(3, rogue.PrimAttributes.Strength);
            Assert.Equal(10, rogue.PrimAttributes.Dexterity);
            Assert.Equal(2, rogue.PrimAttributes.Intelligence);
        }
        [Fact]
        public void Test_Ranger_state_and_return_initial_state()
        {
            Ranger ranger = new Ranger("Ranger");
            Assert.Equal(8, ranger.PrimAttributes.Vitality);
            Assert.Equal(1, ranger.PrimAttributes.Strength);
            Assert.Equal(7, ranger.PrimAttributes.Dexterity);
            Assert.Equal(1, ranger.PrimAttributes.Intelligence);
        }
        [Fact]
        public void Test_Ranger_levelUp_state_and_return_levelUp_state()
        {
            Ranger ranger = new Ranger("Ranger");
            ranger.LevelUp();
            Assert.Equal(10, ranger.PrimAttributes.Vitality);
            Assert.Equal(2, ranger.PrimAttributes.Strength);
            Assert.Equal(12, ranger.PrimAttributes.Dexterity);
            Assert.Equal(2, ranger.PrimAttributes.Intelligence);
        }
        [Fact]
        public void Test_Warrior_State_And_Return_initial_state()
        {
            Warrior warrior = new Warrior("Warrior");
            Assert.Equal(10, warrior.PrimAttributes.Vitality);
            Assert.Equal(5, warrior.PrimAttributes.Strength);
            Assert.Equal(2, warrior.PrimAttributes.Dexterity);
            Assert.Equal(1, warrior.PrimAttributes.Intelligence);
        }
        [Fact]
        public void Test_Warrior_LevelUp_And_Return_LevelUp_state()
        {
            Warrior warrior = new Warrior("Warrior");
            warrior.LevelUp();
            Assert.Equal(15, warrior.PrimAttributes.Vitality);
            Assert.Equal(8, warrior.PrimAttributes.Strength);
            Assert.Equal(4, warrior.PrimAttributes.Dexterity);
            Assert.Equal(2, warrior.PrimAttributes.Intelligence);
        }

    }
}
