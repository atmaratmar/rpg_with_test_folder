﻿using Assignment.Characters;
using Assignment.Characters.Services;
using Assignment.Equipments;
using Assignment.Exceptions;
using Xunit;

namespace AssignmentTest
{
    public class WeaponTest
    {
        [Fact]
        public void Equip_Warrior_Axe_At_Level_One_and_Return_EquipmentException()
        {
            Warrior warrior = new Warrior("Warrior");
            Weapon weapon = new Weapon()
            {
                ItemName = "AXE",
                ItemLevel = 1,
                ItemSlot = Slots.WEAPON,
                WeaponType = WeaponTypes.AXE,
                WeaponAttributes = new WeaponAttributes() {Damage = 9, AttackSpeed = 1.1}
            };
            Assert.Throws<EquipmentException>(() => warrior.SetWeapon(weapon));
        }
        [Fact]

        public void Equip_Warrior_BOW_At_Level_Two_and_Return_Weapon_Added()
        {
            Warrior warrior = new Warrior("Warrior");
            warrior.Level = 2;
            Weapon testAxe = new Weapon()
            {
                ItemName = "BOW",
                ItemLevel = 1,
                ItemSlot = Slots.WEAPON,
                WeaponType = WeaponTypes.BOW,
                WeaponAttributes = new WeaponAttributes() { Damage = 7, AttackSpeed = 1.1 }
            };
            Assert.Equal("Weapon added", warrior.SetWeapon(testAxe));
        }
        [Fact]
        public void Equip_Mage_Axe_At_Level_One_and_Return_EquipmentException()
        {
            Mage mage = new Mage("Mage");
            Weapon weapon = new Weapon()
            {
                ItemName = "STAFF",
                ItemLevel = 1,
                ItemSlot = Slots.WEAPON,
                WeaponType = WeaponTypes.STAFF,
                WeaponAttributes = new WeaponAttributes() { Damage = 9, AttackSpeed = 1.1 }
            };
            Assert.Throws<EquipmentException>(() => mage.SetWeapon(weapon));
        }
        [Fact]
        public void Equip_Mage_BOW_At_Level_Two_and_Return_Weapon_Added()
        {
            Mage mage = new Mage("Mage");
            mage.Level = 2;
            Weapon weapon = new Weapon()
            {
                ItemName = "BOW",
                ItemLevel = 1,
                ItemSlot = Slots.WEAPON,
                WeaponType = WeaponTypes.BOW,
                WeaponAttributes = new WeaponAttributes() { Damage = 7, AttackSpeed = 1.1 }
            };
            Assert.Equal("Weapon added", mage.SetWeapon(weapon));
        }
        [Fact]
        public void Equip_Ranger_Axe_At_Level_One_and_Return_EquipmentException()
        {
            Ranger ranger = new Ranger("Ranger");
            Weapon weapon = new Weapon()
            {
                ItemName = "AXE",
                ItemLevel = 1,
                ItemSlot = Slots.WEAPON,
                WeaponType = WeaponTypes.BOW,
                WeaponAttributes = new WeaponAttributes() { Damage = 9, AttackSpeed = 1.1 }
            };
            Assert.Throws<EquipmentException>(() => ranger.SetWeapon(weapon));
        }
        [Fact]
        public void Equip_Ranger_BOW_At_Level_Two_and_Return_Weapon_Added()
        {
            Ranger ranger = new Ranger("Ranger");
            ranger.Level = 2;
            Weapon weapon = new Weapon()
            {
                ItemName = "AXE",
                ItemLevel = 1,
                ItemSlot = Slots.WEAPON,
                WeaponType = WeaponTypes.AXE,
                WeaponAttributes = new WeaponAttributes() { Damage = 7, AttackSpeed = 1.1 }
            };
            Assert.Equal("Weapon added", ranger.SetWeapon(weapon));
        }
        [Fact]
        public void Equip_Rogue_Axe_At_Level_One_and_Return_EquipmentException()
        {
            Rogue rogue = new Rogue("Rogue");
            Weapon weapon = new Weapon()
            {
                ItemName = "DAGGER",
                ItemLevel = 1,
                ItemSlot = Slots.WEAPON,
                WeaponType = WeaponTypes.DAGGER,
                WeaponAttributes = new WeaponAttributes() { Damage = 9, AttackSpeed = 1.1 }
            };
            Assert.Throws<EquipmentException>(() => rogue.SetWeapon(weapon));
        }
        [Fact]
        public void Equip_Rogue_BOW_At_Level_Two_and_Return_Weapon_Added()
        {
            Rogue rogue = new Rogue("Rogue");
            rogue.Level = 2;
            Weapon weapon = new Weapon()
            {
                ItemName = "BOW",
                ItemLevel = 1,
                ItemSlot = Slots.WEAPON,
                WeaponType = WeaponTypes.BOW,
                WeaponAttributes = new WeaponAttributes() { Damage = 7, AttackSpeed = 1.1 }
            };
            Assert.Equal("Weapon added", rogue.SetWeapon(weapon));
        }
    }
}
