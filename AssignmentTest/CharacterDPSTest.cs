﻿using Assignment.Characters;
using Xunit;

namespace AssignmentTest
{
  public  class CharacterDPSTest
    {
        [Fact]
        public void Character_DPS_For_Warrior_And_Return_Warrior_()
        {
            Warrior warrior = new Warrior("Warrior");
            warrior.CalculateDps();
            double expected = 1 * (1 + (5 / 100.0));
            double actual = warrior.Dps;
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Character_DPS_For_Ranger_And_Return_Ranger_()
        {
            Ranger ranger = new Ranger("Ranger");
            ranger.CalculateDps();
            double expected = 1 * (1 + (1 / 100.0));
            double actual = ranger.Dps;
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Character_DPS_For_Rogue_And_Return_Rogue_()
        {
            Rogue rogue = new Rogue("Rogue");
            rogue.CalculateDps();
            double expected = 1 * (1 + (2 / 100.0));
            double actual = rogue.Dps;
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Character_DPS_For_Mage_And_Return_Mage_()
        {
            Mage mage = new Mage("Mage");
            mage.CalculateDps();
            double expected = 1 * (1 + (1 / 100.0));
            double actual = mage.Dps;
            Assert.Equal(expected, actual);
        }
    }
}
