﻿using Assignment.Characters;
using Xunit;

namespace AssignmentTest
{
   public class SecondaryAtterbutesTest
    {
        [Fact]
        public void Test_Warrior_secondary_And_Return_LevelUp_state()
        {
            Warrior warrior = new Warrior("Warrior");
            warrior.SecondaryAttributesState();
            Assert.Equal(150, warrior.SecondaryAttributes.Health);
            Assert.Equal(12, warrior.SecondaryAttributes.ArmorRating);
            Assert.Equal(2, warrior.SecondaryAttributes.ElementalResistance);
        }

    }
}

