﻿using Assignment.Characters;
using Assignment.Equipments;
using Assignment.Exceptions;
using Xunit;

namespace AssignmentTest
{
    public class HighLevelArmorTest
    {
        [Fact]
        public void Throw_Invalid_Armor_Exception_For_High_Level_Armor()
        {
            Warrior warrior = new Warrior("Warrior");
            Armor testPlateBody = new Armor()
            {
                ItemName = "PLATE",
                ItemLevel = 2,
                ItemSlot = Slots.BODY,
                ArmorType = ArmorTypes.PLATE,
                ArmorAttributes = { Vitality = 2, Strength = 1 }
            };
            Assert.Throws<EquipmentException>(() => warrior.SetArmor(testPlateBody));
        }

    }
}
